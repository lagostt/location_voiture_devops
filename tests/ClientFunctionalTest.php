<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientFunctionalTest extends WebTestCase
{
    public function testShowClient()
    {
        $client = static::createClient();


        $clientId = 1;

        $client->request('GET', '/client/' . $clientId);

        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h1', 'Client');

        $this->assertSelectorTextContains('th', 'Id');
        $this->assertSelectorTextContains('td', $clientId);


        $this->assertSelectorTextContains('th', 'Nom');
        $this->assertSelectorTextContains('td', 'bahri');

        $this->assertSelectorTextContains('th', 'Prenom');
        $this->assertSelectorTextContains('td', 'saif eddine');

        $this->assertSelectorTextContains('th', 'Adresse');
        $this->assertSelectorTextContains('td', 'ben arous');

        $this->assertSelectorTextContains('th', 'Cin');
        $this->assertSelectorTextContains('td', '123000123');

        $this->assertSelectorExists('a[href="/client/"]');
        $this->assertSelectorExists('a[href="/client/' . $clientId . '/edit"]');
        $this->assertSelectorExists('form[action="/client/' . $clientId . '"][method="post"]');
    }

    public function testCreateClient()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/client/new');


        $form = $crawler->selectButton('Save')->form();
        $form['client[nom]'] = 'bahri';
        $form['client[prenom]'] = 'saif eddine';
        $form['client[adresse]'] = 'ben arous';
        $form['client[cin]'] = '123000123';
        $client->submit($form);


        $this->assertResponseRedirects('/client'); // Assuming redirection to the index after creation
    }

    public function testEditClient()
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/client/1/edit');


        $form = $crawler->selectButton('Save')->form();
        $form['client[nom]'] = 'bahri';
        $form['client[prenom]'] = 'saif eddine';
        $form['client[adresse]'] = 'tek up';
        $form['client[cin]'] = '123000123';
        $client->submit($form);


        $this->assertResponseRedirects('/client'); // Assuming redirection to the index after update
    }

    public function testDeleteClient()
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/client/1/delete');


        $form = $crawler->selectButton('Delete')->form();
        $client->submit($form);

        $this->assertResponseRedirects('/client'); // Assuming redirection to the index after deletion
    }

}
