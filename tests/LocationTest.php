<?php

// tests/Entity/LocationTest.php

namespace App\Tests;

use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;


class LocationTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $location = new Location();
        $client = new Client();
        $voiture = new Voiture();

        $dateD = new \DateTime();
        $dateA = new \DateTime();
        $prix = 700.0;

        $location->setDateD($dateD);
        $this->assertEquals($dateD, $location->getDateD());

        $location->setDateA($dateA);
        $this->assertEquals($dateA, $location->getDateA());

        $location->setPrix($prix);
        $this->assertEquals($prix, $location->getPrix());

        $location->setClient($client);
        $this->assertEquals($client, $location->getClient());

        $location->setVoiture($voiture);
        $this->assertEquals($voiture, $location->getVoiture());
    }

    public function testAssociations()
    {
        $location = new Location();
        $client = new Client();
        $voiture = new Voiture();

        $location->setClient($client);
        $this->assertSame($location, $client->getLocations()->first());

        $location->setVoiture($voiture);
        $this->assertSame($location, $voiture->getLocations()->first());
    }
}
