<?php

// tests/Entity/ClientTest.php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $client = new Client();

        $client->setNom('Bahri');
        $this->assertEquals('Bahri', $client->getNom());

        $client->setPrenom('Saif eddine');
        $this->assertEquals('Saif eddine', $client->getPrenom());

        $client->setAdresse('Ben arous');
        $this->assertEquals('Ben arous', $client->getAdresse());

        $client->setCin('123000123');
        $this->assertEquals('123000123', $client->getCin());
    }

    public function testAddRemoveLocation()
    {
        $client = new Client();
        $location = new Location();

        // Add location
        $client->addLocation($location);
        $this->assertTrue($client->getLocations()->contains($location));
        $this->assertSame($client, $location->getClient());

        // Remove location
        $client->removeLocation($location);
        $this->assertFalse($client->getLocations()->contains($location));
        $this->assertNull($location->getClient());
    }
}
