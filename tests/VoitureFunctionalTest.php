<?php

namespace App\Tests;

use App\Entity\Modele;
use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureFunctionalTest extends WebTestCase
{


    public function testVoitureListTemplateRendering()
    {
        $client = static::createClient();


        $crawler = $client->request('GET', '/voiture/list');


        $this->assertResponseIsSuccessful();


        $this->assertSelectorTextContains('h1', 'Liste des voitures');

        $this->assertSelectorExists('table tr td:contains("ID")');
        $this->assertSelectorExists('table tr td:contains("Serie")');
        $this->assertSelectorExists('table tr td:contains("Date")');
        $this->assertSelectorExists('table tr td:contains("Prix Jour")');
        $this->assertSelectorExists('table tr td:contains("Modele)');

        $this->assertSelectorTextContains('td:contains("1")', '1');
        $this->assertSelectorTextContains('td:contains("123")', '123');
        $this->assertSelectorTextContains('td:contains("2023-01-01")', '2023-01-01');
        $this->assertSelectorTextContains('td:contains("700.00")', '700.00');
        $this->assertSelectorTextContains('td:contains("lambo")', 'lambo');

        $this->assertSelectorExists('a[href="/voiture/delete/1"]');
        $this->assertSelectorExists('a[href="/voiture/update/1"]');
    }
    public function testSubmitValidData()
    {
        $client = static::createClient();


        $crawler = $client->request('GET', '/voiture/create');

        $form = $crawler->selectButton('Save')->form();


        $form['voiture_form[serie]'] = '123';
        $form['voiture_form[dateMM]'] = '2023-01-01';
        $form['voiture_form[prixJour]'] = 700.0;
        $form['voiture_form[modele]']='lambo';

        $client->submit($form);


        $this->assertResponseIsSuccessful();




        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
        $voitureRepository = $entityManager->getRepository(Voiture::class);

        $savedVoiture = $voitureRepository->findOneBy([
            'serie' => '123',
            'dateMM' => new \DateTime('2023-01-01'),
            'prixJour' => 700.0,
            'modele' =>'lambo',
        ]);

        $this->assertInstanceOf(Voiture::class, $savedVoiture);
    }
}
