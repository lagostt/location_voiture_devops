<?php

namespace App\Tests;



use App\Entity\Voiture;
use App\Entity\Location;
use App\Entity\Modele;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $voiture = new Voiture();
        $modele = "lambo";

        $serie = '123';
        $dateMM = new \DateTime('2023-01-01');
        $prixJour = 700.0;

        $voiture->setSerie($serie);
        $this->assertEquals($serie, $voiture->getSerie());

        $voiture->setDateMM($dateMM);
        $this->assertEquals($dateMM, $voiture->getDateMM());

        $voiture->setPrixJour($prixJour);
        $this->assertEquals($prixJour, $voiture->getPrixJour());

        $voiture->setModele($modele);
        $this->assertEquals($modele, $voiture->getModele());
    }

    public function testAssociations()
    {
        $voiture = new Voiture();
        $location = new Location();


        $voiture->addLocation($location);
        $this->assertSame($voiture, $location->getVoiture());

    }
}

