<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationFunctionalTest extends WebTestCase
{
    public function testLocationTemplateRendering()
    {
        $client = static::createClient();


        $crawler = $client->request('GET', '/location/1');


        $this->assertResponseIsSuccessful();


        $this->assertSelectorTextContains('h1', 'Location');


        $this->assertSelectorTextContains('th:contains("Id")', 'Id');
        $this->assertSelectorTextContains('td:contains("1")', '1');

        $this->assertSelectorTextContains('th:contains("DateD")', 'DateD');
        $this->assertSelectorTextContains('td:contains("2023-01-01")', '2023-01-01');

        $this->assertSelectorTextContains('th:contains("DateA")', 'DateA');
        $this->assertSelectorTextContains('td:contains("2023-01-05")', '2023-01-05');

        $this->assertSelectorTextContains('th:contains("Prix")', 'Prix');
        $this->assertSelectorTextContains('td:contains("700.00")', '700.00');


        $this->assertSelectorExists('a[href="/location/index"]');


        $this->assertSelectorExists('a[href="/location/1/edit"]');


        $this->assertSelectorExists('form[action="/location/1/delete"][method="post"]');
    }
    public function testSubmitValidData()
    {
        $client = static::createClient();


        $crawler = $client->request('GET', '/location/create');

        $form = $crawler->selectButton('Save')->form();


        $form['location[dateD]'] = '2023-01-01';
        $form['location[dateA]'] = '2023-01-05';
        $form['location[client]']->select(1);
        $form['location[voiture]']->select(1);

        $client->submit($form);


        $this->assertResponseIsSuccessful();


        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();
        $locationRepository = $entityManager->getRepository(Location::class);

        $savedLocation = $locationRepository->findOneBy([
            'dateD' => new \DateTime('2023-01-01'),
            'dateA' => new \DateTime('2023-01-05'),
            'client' => $entityManager->getReference(Client::class, 1),
            'voiture' => $entityManager->getReference(Voiture::class, 1),
        ]);

        $this->assertInstanceOf(Location::class, $savedLocation);
    }
}
